package br.mp.mpto.fragmentsexample;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Main extends AppCompatActivity {

    private FragmentManager manager = null;
    private FragmentTransaction transaction = null;
    private FragmentA fragmentA = null;
    private FragmentB fragmentB = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);

        manager = getSupportFragmentManager();
        fragmentA = new FragmentA();
        transaction = manager.beginTransaction();
        transaction.replace(R.id.main_frameLayout, fragmentA);
        transaction.commit();
    }

    public void handleGoFragmentB(View button) {
        fragmentB = new FragmentB();
        transaction = manager.beginTransaction();
        transaction.replace(R.id.main_frameLayout, fragmentB);
        transaction.addToBackStack("AB Screen");
        transaction.commit();
    }
}
