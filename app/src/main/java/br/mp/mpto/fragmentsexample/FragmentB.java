package br.mp.mpto.fragmentsexample;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentB extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_fragment_b, container, false);
        Log.i("INFO", "Fragment B created");

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.i("INFO", "Fragment B coupled");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.i("INFO", "Fragment B decoupled");
    }
}
